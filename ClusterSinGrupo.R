load(file="contactos.el.txt")

#REMOVER INTEGRANTES DEL GRUPO (ojo que se pierde DKR, queda desconexo)
grupo = c("MAB", "LDP", "MGT")
df = as.data.frame(contactos.el)
df = rbind(df, as.data.frame(cbind("DKR", "DKR")))
contactos.singrupo = as.matrix(df[(df[,1] %in% grupo | df[,2] %in% grupo) == FALSE,])
contactos.el = contactos.singrupo
contactos.congrupo <-as.matrix(df)

library("igraph")
redcg <-  simplify(graph.edgelist(contactos.congrupo, directed=T))
red <- simplify(graph.edgelist(contactos.el, directed=T))

### Walktrap - community
## This function tries to find densely connected subgraphs, 
## also called communities in a graph via random walks. 
## The idea is that short random walks tend to stay in the same community.
red.wc.cm <- walktrap.community(red)
redcg.wc.cm <- walktrap.community(redcg)
red.wc.cm

V(redcg)$grupo <- redcg.wc.cm$membership
V(red)$grupo <- red.wc.cm$membership

cluster.col <- c("white", "cyan", "yellow", "green", "azure3", "red", "bisque4", "cyan4", "magenta")
for (i in 1:max(V(red)$grupo)) V(red)[grupo==i]$color <- cluster.col[i]
plot(red, vertex.color=V(red)$color, edge.arrow.size=0.3)

for (i in 1:max(V(redcg)$grupo)) V(redcg)[grupo==i]$color <- cluster.col[i]
plot(redcg, vertex.color=V(redcg)$color, edge.arrow.size=0.3)

miembroscg <- data.frame(cbind(V(redcg)$name,V(redcg)$grupo))
names(miembroscg)<-c('ID','CluswtCG')

miembrossg <- data.frame(cbind(V(red)$name,V(red)$grupo))
names(miembrossg)<-c('ID','CluswtSG')

if ( is.connected(red, mode="weak") ) {
  red.connected <- red
} else {
  # analisis sin el nodo DKR, no esta conectado al resto 
  red.connected <- graph.edgelist(contactos.el[contactos.el[,1] != "DKR",], directed=T)
}

if ( is.connected(redcg, mode="weak") ) {
  redcg.connected <- redcg
} else {
  # analisis sin el nodo DKR, no esta conectado al resto 
  redcg.connected <- graph.edgelist(contactos.congrupo[contactos.congrupo[,1] != "DKR",], directed=T)
}

redcg.sg.cm <- spinglass.community(redcg.connected,spins=8)
red.sg.cm <- spinglass.community(red.connected,spins=8)

V(redcg.connected)$grupo <- redcg.sg.cm$membership
V(red.connected)$grupo <- red.sg.cm$membership

miembroscgsg <- data.frame(cbind(V(redcg.connected)$name,V(redcg.connected)$grupo))
names(miembroscgsg)<-c('ID','CluswtCGsg')
miembrossgsg <- data.frame(cbind(V(red.connected)$name,V(red.connected)$grupo))
names(miembrossgsg)<-c('ID','CluswtSGsg')

miembros <- merge(miembroscg,miembrossg, by.y="ID")
miembros <- merge(miembros,miembrossgsg, by.x="ID")
miembros <- merge(miembros,miembroscgsg, by.x="ID")

miembros$CluswtSG<-factor(miembros$CluswtSG, exclude=5)

table(miembros[,c('CluswtCG','CluswtSG')])



table(miembros[,c('CluswtCG','CluswtSG')])
table(miembros[,c('CluswtCGsg','CluswtSGsg')])

cluster.col <- c("white", "cyan", "yellow", "green", "azure3", "red", "bisque4", "cyan4", "magenta")
for (i in 1:max(V(red)$grupo)) V(red)[grupo==i]$color <- cluster.col[i]
plot(red, vertex.color=V(red)$color, edge.arrow.size=0.3)
for (i in 1:max(V(redcg)$grupo)) V(redcg)[grupo==i]$color <- cluster.col[i]
plot(redcg, vertex.color=V(redcg)$color, edge.arrow.size=0.3)

for (i in 1:max(V(red.connected)$grupo)) V(red.connected)[grupo==i]$color <- cluster.col[i]
plot(red.connected, vertex.color=V(red.connected)$color, edge.arrow.size=0.3)
